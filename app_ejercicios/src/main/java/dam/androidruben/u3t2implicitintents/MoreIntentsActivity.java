package dam.androidruben.u3t2implicitintents;

import static com.google.android.gms.actions.NoteIntents.ACTION_CREATE_NOTE;
import static com.google.android.gms.actions.NoteIntents.EXTRA_NAME;
import static com.google.android.gms.actions.NoteIntents.EXTRA_TEXT;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.actions.ReserveIntents;

public class MoreIntentsActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicitsIntents";

    private EditText etSubject, etNoteText, etPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);

        setUI();
    }

    private void setUI() {
        Button btCreateNote, btCall, btCallTaxi;

        etSubject = findViewById(R.id.etSubject);
        etNoteText = findViewById(R.id.etNoteText);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);

        btCreateNote = findViewById(R.id.btCreateNote);
        btCall = findViewById(R.id.btCall);
        btCallTaxi = findViewById(R.id.btCallTaxi);

        btCreateNote.setOnClickListener(this);
        btCall.setOnClickListener(this);
        btCallTaxi.setOnClickListener(this);
    }

    public void createNote(String subject, String text) {
        Intent intent = new Intent(ACTION_CREATE_NOTE)
                .putExtra(EXTRA_NAME, subject)
                .putExtra(EXTRA_TEXT, text);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS,"createNote: Can't handle this intent!");
        }
    }

    public void callPhone(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel:", phone, null));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS,"callPhone: Can't handle this intent!");
        }
    }

    public void callTaxi() {
        Intent intent = new Intent(ReserveIntents.ACTION_RESERVE_TAXI_RESERVATION);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS,"callTaxi: Can't handle this intent!");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btCreateNote:
                createNote(etSubject.getText().toString(),etNoteText.getText().toString());
                break;
            case R.id.btCall:
                callPhone(etPhoneNumber.getText().toString().trim());
                break;
            case R.id.btCallTaxi:
                callTaxi();
                break;
        }
    }
}
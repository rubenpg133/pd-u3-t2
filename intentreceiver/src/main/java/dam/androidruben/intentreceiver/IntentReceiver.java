package dam.androidruben.intentreceiver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.os.Bundle;
import android.widget.TextView;

public class IntentReceiver extends AppCompatActivity {

    private TextView tvTextReceived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_receiver);

        setUI();
    }

    private void setUI() {
        tvTextReceived = findViewById(R.id.tvTextReceived);

        ShareCompat.IntentReader intentReader =
                new ShareCompat.IntentReader(this);
        if (intentReader.isShareIntent()) {
            String text = (String) intentReader.getText();
            tvTextReceived.setText(text);
        }
    }
}
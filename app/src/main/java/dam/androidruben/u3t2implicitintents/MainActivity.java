package dam.androidruben.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String IMPLICIT_INTENTS = "ImplicitsIntents";

    private EditText etUri, etLocation, etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        Button btOpenUri, btOpenLocation, btShareText;

        etUri = findViewById(R.id.etUri);
        etLocation = findViewById(R.id.etLocation);
        etText = findViewById(R.id.etText);

        btOpenUri = findViewById(R.id.btUri);
        btOpenLocation = findViewById(R.id.btLocation);
        btShareText = findViewById(R.id.btText);

        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShareText.setOnClickListener(this);
    }

    private void openWebsite(String uriText) {
        Uri webPage = Uri.parse(uriText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webPage);

        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS,"openWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location) {
        Uri addressUri = Uri.parse("geo:0,0?q=" + location);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS,"openLocation: Can't handle this intent!");
        }
    }

    private void shareText(String text) {
        new ShareCompat.IntentBuilder(this)
                .setType("text/plan")
                .setText(text)
                .startChooser();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btUri:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.btLocation:
                openLocation(etLocation.getText().toString());
                break;
            case R.id.btText:
                shareText(etText.getText().toString());
                break;
        }
    }
}